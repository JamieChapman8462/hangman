package hangman.service;

import hangman.model.User;

public interface UserService {

    void save(User user);

    void autoLogin(User user);

    User findByUsername(String username);
}
