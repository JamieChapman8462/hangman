package hangman.service;

import hangman.model.Game;
import hangman.model.User;

public interface GameService {

    void updateGame(Game game, char character);

    Game getGame(User user);

    Game newGame(User user);

    Iterable<Game> list();
}
