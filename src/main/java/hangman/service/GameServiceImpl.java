package hangman.service;

import hangman.model.Game;
import hangman.model.GameRepository;
import hangman.model.GameState;
import hangman.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.thymeleaf.util.StringUtils;

import java.util.Arrays;
import java.util.List;
import java.util.Random;

@Service
public class GameServiceImpl implements GameService {

    private static List<String> words = Arrays.asList("c++", "java", "javascript", "python", "groovy");

    @Autowired
    private GameRepository gameRepository;

    @Override
    public void updateGame(Game game, char character) {
        int characterMatches = updateDisplayWord(game, character);
        updateAttemptsLeft(characterMatches, game);
        updateGameState(game);
        gameRepository.save(game);
    }

    @Override
    public Game getGame(User user) {
        Game game = gameRepository.findByUsernameAndGameStateActive(user.getUsername());
        return game != null ? game : newGame(user);
    }

    @Override
    public Game newGame(User user) {
        Game game = new Game(user, 11, getWord(), GameState.ACTIVE);
        initialiseDisplayedWord(game);
        return gameRepository.save(game);
    }

    @Override
    public Iterable<Game> list() {
        return gameRepository.findAll();
    }

    private int updateDisplayWord(Game game, char character) {
        int characterMatches = 0;
        for (int i = 0; i < game.getWord().length; i++) {
            if (game.getWord()[i] == character) {
                game.getDisplayedWord()[i] = character;
                characterMatches++;
            }
        }
        return characterMatches;
    }

    private void updateGameState(Game game) {
        if (Arrays.equals(game.getDisplayedWord(), game.getWord())) {
            game.setGameState(GameState.SUCCESS);
        }
        else {
            if (game.getAttemptsLeft() == 0) {
                game.setGameState(GameState.FAILED);
            }
            else {
                game.setGameState(GameState.ACTIVE);
            }
        }
    }

    private char[] getWord() {
        return words.get(new Random().nextInt(4)).toCharArray();
    }

    private void initialiseDisplayedWord(Game game) {
        game.setDisplayedWord(StringUtils.repeat("*", game.getWord().length).toCharArray());
    }

    private void updateAttemptsLeft(int characterMatches, Game game) {
        if (characterMatches == 0) {
            game.setAttemptsLeft(game.getAttemptsLeft() - 1);
        }
    }
}
