package hangman.model;

import javax.persistence.*;

@Entity
public class Game {

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private Integer id;

    @ManyToOne
    @JoinColumn(name = "user_id")
    private User user;

    private Integer attemptsLeft;
    private char[] word;
    private char[] displayedWord;
    @Enumerated(EnumType.STRING)
    private GameState gameState;

    public Game(User user, Integer attemptsLeft, char[] word, GameState gameState) {
        this.user = user;
        this.attemptsLeft = attemptsLeft;
        this.word = word;
        this.gameState = gameState;
    }

    public Game() {
    }

    public Integer getAttemptsLeft() {
        return attemptsLeft;
    }
    public void setAttemptsLeft(Integer attemptsLeft) {
        this.attemptsLeft = attemptsLeft;
    }

    public char[] getWord() {
        return word;
    }

    public void setWord(char[] word) {
        this.word = word;
    }

    public char[] getDisplayedWord() {
        return displayedWord;
    }

    public void setDisplayedWord(char[] displayedWord) {
        this.displayedWord = displayedWord;
    }

    public GameState getGameState() {
        return gameState;
    }

    public void setGameState(GameState gameState) {
        this.gameState = gameState;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Integer getId() {
        return id;
    }
}
