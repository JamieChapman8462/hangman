package hangman.model;

public enum GameState {

    SUCCESS, ACTIVE, FAILED;

    @Override
    public String toString() {
        return this.name().toLowerCase();
    }
}
