package hangman.model;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

public interface GameRepository extends CrudRepository<Game, Integer> {

    @Query("select g from Game g where g.user.username = :username and g.gameState = 'ACTIVE'")
    Game findByUsernameAndGameStateActive(@Param("username") String username);
}
