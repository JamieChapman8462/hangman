package hangman.controllers;

import hangman.model.GameState;
import hangman.model.Role;
import hangman.model.User;
import hangman.service.GameService;
import hangman.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

@Controller
public class UserController {

    @Autowired
    private UserService userService;

    @Autowired
    private GameService gameService;

    @GetMapping("/login")
    public String loginForm() {
        return "login";
    }

    @GetMapping("/user/register")
    public String registerForm() {
        return "register";
    }

    @PostMapping("/user/register")
    public String createUser(@ModelAttribute User user, Model model) {
        user.setRole(Role.USER);
        userService.save(user);
        userService.autoLogin(user);
        model.addAttribute("game", gameService.newGame(user));
        return GameState.ACTIVE.toString();
    }
}
