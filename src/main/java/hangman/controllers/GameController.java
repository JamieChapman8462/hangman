package hangman.controllers;

import hangman.model.Game;
import hangman.model.GameState;
import hangman.service.GameService;
import hangman.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

@Controller
public class GameController {

    @Autowired
    private GameService gameService;

    @Autowired
    private UserService userService;

    @PostMapping(path = "/user/game/update")
    public String update(@ModelAttribute Game game, @RequestParam char character) {
        gameService.updateGame(game, character);
        return game.getGameState().toString();
    }

    @GetMapping(path = "/user/game/active")
    public String active(Model model, Authentication authentication) {
        UserDetails currentUser = (UserDetails) authentication.getPrincipal();
        model.addAttribute("game", gameService.getGame(userService.findByUsername(currentUser.getUsername())));
        return GameState.ACTIVE.toString();
    }

    @GetMapping(path = "/admin/game/list")
    public String list(Model model) {
        model.addAttribute("games", gameService.list());
        return "list";
    }
}
