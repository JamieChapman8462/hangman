package hangman.service;

import hangman.model.Role;
import hangman.model.User;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
public class userDetailsServiceImplTest extends TestUtils{

    @Autowired
    private UserDetailsService userDetailsService;

    @Test
    public void checkUserHasCorrectAuthorises() {
        createUser();
        User user = getUser();
        UserDetails userDetails = userDetailsService.loadUserByUsername(user.getUsername());
        Assert.assertEquals(userDetails.getAuthorities().size(), 1);
        Assert.assertTrue(userDetails.getAuthorities().contains(new SimpleGrantedAuthority(Role.USER.name())));
    }
}
