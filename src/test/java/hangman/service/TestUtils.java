package hangman.service;

import hangman.model.Role;
import hangman.model.User;
import org.springframework.beans.factory.annotation.Autowired;

class TestUtils {

    @Autowired
    private UserService userService;

    private static final String USERNAME = "Nexmo";
    private User user;

    void createUser() {
        user =  new User();
        user.setUsername(USERNAME);
        user.setPassword("test");
        user.setRole(Role.USER);
        userService.save(user);
    }

    User getUser() {
        return user;
    }
}
