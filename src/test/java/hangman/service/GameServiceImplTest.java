package hangman.service;

import hangman.model.*;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringRunner;
import java.util.Arrays;

@RunWith(SpringRunner.class)
@SpringBootTest
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
public class GameServiceImplTest extends TestUtils {

    @Autowired
    GameService gameService;

    @Autowired
    GameRepository gameRepository;

    private Game game;

    @Before
    public void Setup() {
        createUser();
        createGame();
    }

    @Test
    public void gameIsCreated() {
        Assert.assertEquals(game.getDisplayedWord().length, game.getWord().length);
    }

    @Test
    public void gameIsCreatedWhenOneExistsButIsNotActive() {
        game.setGameState(GameState.FAILED);
        gameRepository.save(game);
        Game newGame = gameService.getGame(getUser());
        Assert.assertNotEquals(game.getId(), newGame.getId());
    }

    @Test
    public void gameIsNotCreatedWhenOneExistsAndIsActive() {
        Game newGame = gameService.getGame(getUser());
        Assert.assertEquals(game.getId(), newGame.getId());
    }

    @Test
    public void attemptsLeftIsUpdatedWhenCharacterIsNotPresent() {
        gameService.updateGame(game, 'q');
        game = gameService.getGame(getUser());
        Assert.assertEquals(game.getAttemptsLeft(), (Integer) 10);
    }

    @Test
    public void attemptsLeftIsUpdatedWhenCharacterIsPresent() {
        gameService.updateGame(game, 't');
        game = gameService.getGame(getUser());
        Assert.assertEquals(game.getAttemptsLeft(), (Integer) 11);
    }

    @Test
    public void displayedWordIsUpdatedWhenCharacterIsPresent() {
        gameService.updateGame(game, 't');
        game = gameService.getGame(getUser());
        Assert.assertEquals(Arrays.toString(game.getDisplayedWord()), Arrays.toString(new char[]{'t', '*', '*', 't'}));
    }

    @Test
    public void gameStatusIsCorrectWhenNoAttemptsLeft() {
        game.setAttemptsLeft(1);
        gameRepository.save(game);
        gameService.updateGame(game, 'q');
        game = gameRepository.findOne(game.getId());
        Assert.assertEquals(game.getGameState(), GameState.FAILED);
    }

    @Test
    public void gameStatusIsCorrectWhenAttemptsLeft() {
        game.setAttemptsLeft(2);
        gameRepository.save(game);
        gameService.updateGame(game, 'q');
        game = gameRepository.findOne(game.getId());
        Assert.assertEquals(game.getGameState(), GameState.ACTIVE);
    }

    @Test
    public void gameStatusIsCorrectWhenSuccessful() {
        game.setDisplayedWord(new char[]{'t', '*', 's', 't'});
        game.setAttemptsLeft(2);
        gameRepository.save(game);
        gameService.updateGame(game, 'e');
        game = gameRepository.findOne(game.getId());
        Assert.assertEquals(game.getGameState(), GameState.SUCCESS);
    }

    private void createGame() {
        game = gameService.newGame(getUser());
        game.setWord(new char[]{'t', 'e', 's', 't'});
        game.setDisplayedWord(new char[]{'*', '*', '*', '*'});
        gameRepository.save(game);
    }
}
