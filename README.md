# Project Name

Hangman

## About

This is a game of hangman, upon starting you will need to register a user, a game will then commence were you have 11 opportunities to guess the word.

## Installation

    git clone https://JamieChapman8462@bitbucket.org/JamieChapman8462/hangman.git

## Usage

    cd <project-directory>
    ./gradlew clean build
    cd build/libs/
    java -jar nexmo-hangman-0.1.0.jar

## Access

You have the option to login as Admin, this will give you access to all the games and their various states.

The credentials are:

    username: admin
    password: admin